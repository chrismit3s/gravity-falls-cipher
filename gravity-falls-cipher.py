#!/usr/bin/env python3

# TODO
#   

class Cipher():
    def __init__(self, file, season=1, episode=1, number=1, alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ"):
        # copy alphabet and filepath as strings are immutable
        self.cipher_file = str(file)
        self.alphabet = str(alphabet)

        # set default values + ranges
        with open(self.cipher_file) as file:
            for line in file:
                if line[0] == "#":
                    s, e, n = line[1:].split("-")
                    break
        self.file_ranges = ([1, int(s) + 1], [1, int(e) + 1], [1, int(n) + 1])
        self.method_ranges = ([False, True], [False, True], list(range(len(alphabet))), None)
        self.default_method = (False, False, 0, "")

        # read the cipher from the file
        self.get_cipher(season, episode, number)

        # create dict for dechiphered texts
        self.deciphered = {self.default_method: (self.ciphered, self.score_cipher(self.ciphered))}

    def get_cipher(self, season=1, episode=1, number=1): # return the cipher in a episode
        self.season, self.episode, self.number = season, episode, number
        key = f"S{season:02}E{episode:02}N{number:02}"

        with open(self.cipher_file) as file:
            # find line matching the key
            for line in file:
                if key == line[:9]:
                    # return line without newline: key (0:8) + seperating space (9) + ending newline [if there] (1)
                    if line[-1] == "\n":
                        self.ciphered = line[10:-1]
                        return line[10:-1]
                    else:
                        self.ciphered = line[10:]
                        return line[10:]
        # no line matches
        self.ciphered = ""
        return ""
    def append_cipher(self, method, text): # append the cipher to the instance dict
        if method not in self.deciphered:
            self.deciphered[method] = (text, self.score_cipher(text))
        elif self.deciphered[method][0] != text:
            raise KeyError(f"Contradiction in deciphered values; same method, different texts; method is {method}; texts are '{self.deciphered[method][0]}' and '{text}'")
        pass
    def print_cipher(self, method=None): # print all information about a deciphering method or the header for a table
        # print header for table
        if method == None:
            # print header
            print("Key      ", end=" | ")
            print("Score", end=" | ")
            print("A1z26 ", end=" | ")
            print("Atbash", end=" | ")
            print("Ceasar", end=" | ")
            print("Beaufort        ", end=" | ")
            print("Deciphered Text")

            # print underline
            print("-"*9, end="-+-")
            print("-"*5, end="-+-")
            print("-"*6, end="-+-")
            print("-"*6, end="-+-")
            print("-"*6, end="-+-")
            print("-"*16, end="-+-")
            print("-"*20)
        # print cipher
        else:
            self.decipher(method)
            # key (season, episode, number)
            print(f"S{self.season:02}E{self.episode:02}N{self.number:02}", end=" | ")
            # value
            print(f"{self.deciphered[method][1]:5}", end=" | ")
            # methods
            print(f"{method[0]:6}", end=" | ")
            print(f"{method[1]:6}", end=" | ")
            print(f"{method[2]:6}", end=" | ")
            print(f"{method[3]:<16}", end=" | ")
            # text
            print(f"{self.decipher(method)[0]}")
    def score_cipher(self, text): # count how many real words are in the deciphered text
        # common words for valuing
        word_list = ["gravity", "falls",
            # character names
            "dipper", "mabel", "stanley", "stanfort", "wendy", "soos", "bill", "ciphers",
            # 1000 most common english words
            "the", "of", "to", "and", "a", "in", "is", "it", "you", "that", "he", "was", "for", "on", "are", "with", "as", "I", "his", "they", "be", "at", "one", "have", "this", "from", "or", "had", "by", "hot", "word", "but", "what", "some", "we", "can", "out", "other", "were", "all", "there", "when", "up", "use", "your", "how", "said", "an", "each", "she", "which", "do", "their", "time", "if", "will", "way", "about", "many", "then", "them", "write", "would", "like", "so", "these", "her", "long", "make", "thing", "see", "him", "two", "has", "look", "more", "day", "could", "go", "come", "did", "number", "sound", "no", "most", "people", "my", "over", "know", "water", "than", "call", "first", "who", "may", "down", "side", "been", "now", "find", "any", "new", "work", "part", "take", "get", "place", "made", "live", "where", "after", "back", "little", "only", "round", "man", "year", "came", "show", "every", "good", "me", "give", "our", "under", "name", "very", "through", "just", "form", "sentence", "great", "think", "say", "help", "low", "line", "differ", "turn", "cause", "much", "mean", "before", "move", "right", "boy", "old", "too", "same", "tell", "does", "set", "three", "want", "air", "well", "also", "play", "small", "end", "put", "home", "read", "hand", "port", "large", "spell", "add", "even", "land", "here", "must", "big", "high", "such", "follow", "act", "why", "ask", "men", "change", "went", "light", "kind", "off", "need", "house", "picture", "try", "us", "again", "animal", "point", "mother", "world", "near", "build", "self", "earth", "father", "head", "stand", "own", "page", "should", "country", "found", "answer", "school", "grow", "study", "still", "learn", "plant", "cover", "food", "sun", "four", "between", "state", "keep", "eye", "never", "last", "let", "thought", "city", "tree", "cross", "farm", "hard", "start", "might", "story", "saw", "far", "sea", "draw", "left", "late", "run", "don't", "while", "press", "close", "night", "real", "life", "few", "north", "open", "seem", "together", "next", "white", "children", "begin", "got", "walk", "example", "ease", "paper", "group", "always", "music", "those", "both", "mark", "often", "letter", "until", "mile", "river", "car", "feet", "care", "second", "book", "carry", "took", "science", "eat", "room", "friend", "began", "idea", "fish", "mountain", "stop", "once", "base", "hear", "horse", "cut", "sure", "watch", "color", "face", "wood", "main", "enough", "plain", "girl", "usual", "young", "ready", "above", "ever", "red", "list", "though", "feel", "talk", "bird", "soon", "body", "dog", "family", "direct", "pose", "leave", "song", "measure", "door", "product", "black", "short", "numeral", "class", "wind", "question", "happen", "complete", "ship", "area", "half", "rock", "order", "fire", "south", "problem", "piece", "told", "knew", "pass", "since", "top", "whole", "king", "space", "heard", "best", "hour", "better", "true", "during", "hundred", "five", "remember", "step", "early", "hold", "west", "ground", "interest", "reach", "fast", "verb", "sing", "listen", "six", "table", "travel", "less", "morning", "ten", "simple", "several", "vowel", "toward", "war", "lay", "against", "pattern", "slow", "center", "love", "person", "money", "serve", "appear", "road", "map", "rain", "rule", "govern", "pull", "cold", "notice", "voice", "unit", "power", "town", "fine", "certain", "fly", "fall", "lead", "cry", "dark", "machine", "note", "wait", "plan", "figure", "star", "box", "noun", "field", "rest", "correct", "able", "pound", "done", "beauty", "drive", "stood", "contain", "front", "teach", "week", "final", "gave", "green", "oh", "quick", "develop", "ocean", "warm", "free", "minute", "strong", "special", "mind", "behind", "clear", "tail", "produce", "fact", "street", "inch", "multiply", "nothing", "course", "stay", "wheel", "full", "force", "blue", "object", "decide", "surface", "deep", "moon", "island", "foot", "system", "busy", "test", "record", "boat", "common", "gold", "possible", "plane", "stead", "dry", "wonder", "laugh", "thousand", "ago", "ran", "check", "game", "shape", "equate", "hot", "miss", "brought", "heat", "snow", "tire", "bring", "yes", "distant", "fill", "east", "paint", "language", "among", "grand", "ball", "yet", "wave", "drop", "heart", "am", "present", "heavy", "dance", "engine", "position", "arm", "wide", "sail", "material", "size", "vary", "settle", "speak", "weight", "general", "ice", "matter", "circle", "pair", "include", "divide", "syllable", "felt", "perhaps", "pick", "sudden", "count", "square", "reason", "length", "represent", "art", "subject", "region", "energy", "hunt", "probable", "bed", "brother", "egg", "ride", "cell", "believe", "fraction", "forest", "sit", "race", "window", "store", "summer", "train", "sleep", "prove", "lone", "leg", "exercise", "wall", "catch", "mount", "wish", "sky", "board", "joy", "winter", "sat", "written", "wild", "instrument", "kept", "glass", "grass", "cow", "job", "edge", "sign", "visit", "past", "soft", "fun", "bright", "gas", "weather", "month", "million", "bear", "finish", "happy", "hope", "flower", "clothe", "strange", "gone", "jump", "baby", "eight", "village", "meet", "root", "buy", "raise", "solve", "metal", "whether", "push", "seven", "paragraph", "third", "shall", "held", "hair", "describe", "cook", "floor", "either", "result", "burn", "hill", "safe", "cat", "century", "consider", "type", "law", "bit", "coast", "copy", "phrase", "silent", "tall", "sand", "soil", "roll", "temperature", "finger", "industry", "value", "fight", "lie", "beat", "excite", "natural", "view", "sense", "ear", "else", "quite", "broke", "case", "middle", "kill", "son", "lake", "moment", "scale", "loud", "spring", "observe", "child", "straight", "consonant", "nation", "dictionary", "milk", "speed", "method", "organ", "pay", "age", "section", "dress", "cloud", "surprise", "quiet", "stone", "tiny", "climb", "cool", "design", "poor", "lot", "experiment", "bottom", "key", "iron", "single", "stick", "flat", "twenty", "skin", "smile", "crease", "hole", "trade", "melody", "trip", "office", "receive", "row", "mouth", "exact", "symbol", "die", "least", "trouble", "shout", "except", "wrote", "seed", "tone", "join", "suggest", "clean", "break", "lady", "yard", "rise", "bad", "blow", "oil", "blood", "touch", "grew", "cent", "mix", "team", "wire", "cost", "lost", "brown", "wear", "garden", "equal", "sent", "choose", "fell", "fit", "flow", "fair", "bank", "collect", "save", "control", "decimal", "gentle", "woman", "captain", "practice", "separate", "difficult", "doctor", "please", "protect", "noon", "whose", "locate", "ring", "character", "insect", "caught", "period", "indicate", "radio", "spoke", "atom", "human", "history", "effect", "electric", "expect", "crop", "modern", "element", "hit", "student", "corner", "party", "supply", "bone", "rail", "imagine", "provide", "agree", "thus", "capital", "won't", "chair", "danger", "fruit", "rich", "thick", "soldier", "process", "operate", "guess", "necessary", "sharp", "wing", "create", "neighbor", "wash", "bat", "rather", "crowd", "corn", "compare", "poem", "string", "bell", "depend", "meat", "rub", "tube", "famous", "dollar", "stream", "fear", "sight", "thin", "triangle", "planet", "hurry", "chief", "colony", "clock", "mine", "tie", "enter", "major", "fresh", "search", "send", "yellow", "gun", "allow", "print", "dead", "spot", "desert", "suit", "current", "lift", "rose", "continue", "block", "chart", "hat", "sell", "success", "company", "subtract", "event", "particular", "deal", "swim", "term", "opposite", "wife", "shoe", "shoulder", "spread", "arrange", "camp", "invent", "cotton", "born", "determine", "quart", "nine", "truck", "noise", "level", "chance", "gather", "shop", "stretch", "throw", "shine", "property", "column", "molecule", "select", "wrong", "gray", "repeat", "require", "broad", "prepare", "salt", "nose", "plural", "anger", "claim", "continent", "oxygen", "sugar", "death", "pretty", "skill", "women", "season", "solution", "magnet", "silver", "thank", "branch", "match", "suffix", "especially", "fig", "afraid", "huge", "sister", "steel", "discuss", "forward", "similar", "guide", "experience", "score", "apple", "bought", "led", "pitch", "coat", "mass", "card", "band", "rope", "slip", "win", "dream", "evening", "condition", "feed", "tool", "total", "basic", "smell", "valley", "nor", "double", "seat", "arrive", "master", "track", "parent", "shore", "division", "sheet", "substance", "favor", "connect", "post", "spend", "chord", "fat", "glad", "original", "share", "station", "dad", "bread", "charge", "proper", "bar", "offer", "segment", "slave", "duck", "instant", "market", "degree", "populate", "chick", "dear", "enemy", "reply", "drink", "occur", "support", "speech", "nature", "range", "steam", "motion", "path", "liquid", "log", "meant", "quotient", "teeth", "shell", "neck",
            # numbers written out
            "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve",
            # others, to bias certain ciphers 
            "not", "approved", "onwards", "pluribus", "trembley", "reverse", "patos", "piscina"]

        # get rid of special characters
        for char in text:
            if char not in (self.alphabet + " "):
                text = text.replace(char, " ")
        text = " " + text + " "

        # count words
        count = 0
        for word in word_list:
            if (" " + word.upper() + " ") in text:
                count += 1
        return count
    def best_cipher(self): # return method for deciphered with best score
        # decipher all methods
        self.decipher_all()
        
        # return method with max score
        return max(self.deciphered, key=lambda x: self.deciphered[x][1])

    def get_key(self, season=1, episode=1, number=1): # return the key in a episode
        key = f"S{season:02}E{episode:02}K{number:02}"

        with open(self.cipher_file) as file:
            # find line matching the key
            for line in file:
                if key == line[:9]:
                    # return line without newline: key (0:8) + seperating space (9) + ending newline [if there] (1)
                    if line[-1] == "\n":
                        return line[10:-1]
                    else:
                        return line[10:]
        # no line matches
        return ""
    def iter_keys(self): # return all keys in the cipher file
        with open(self.cipher_file) as file:
            # find line matching the key
            for line in file:
                if "K" in line[:9]:
                    # return line without newline: key (0:8) + seperating space (9) + ending newline [if there] (1)
                    if line[-1] == "\n":
                        yield line[10:-1]
                    else:
                        yield line[10:]
        yield ""

    def iter_methods(self): # iterate over all possible methods (doesnt bruteforce beaufort key)
       for beaufort in self.iter_keys():
           for a1z26 in self.method_ranges[0]:
               for atbash in self.method_ranges[1]:
                   for ceasar in self.method_ranges[2]:
                       yield (a1z26, atbash, ceasar, beaufort)
       pass
    def rank_methods(self): # return a ordered list of the best to worst deciphering methods
        # decipher all methods
        self.decipher_all()
        
        # convert cipher dict to list of tuples (value, (a1z26, atbash, ceasar, beaufort))
        cipher_list = list((self.deciphered[x][1], x) for x in self.deciphered)

        # sort list after value (which is the first value of the tuple)
        cipher_list.sort(reverse=True)

        # return ordered list
        return list(x[1] for x in cipher_list)

    def decipher(self, method=None):
        # deciphering methods
        def a1z26(in_str, _):
            out_str = ""
            word = ""

            # read every char in in_str (last # to translate last word)
            for char in in_str + "#":
                # append matching characters to translate later
                if char in "0123456789-":
                    word += char

                # translate word or add untranslatable character unchanged
                else:
                    if word != "":
                        for letter in word.split("-"):
                            try:
                                j = int(letter)
                            except ValueError:
                                raise ValueError(f"'{letter}' is not a number")
                            try:
                                out_str += self.alphabet[j - 1]
                            except IndexError:
                                raise IndexError(f"{j} is out of bounds")
                    out_str += char
                    word = ""
            # return string without last #
            return out_str[:-1]
        def atbash(in_str, _):
            out_str = ""
            for char in in_str:
                if char in self.alphabet:
                    char_val = self.alphabet.find(char)
                    out_str += self.alphabet[len(self.alphabet) - char_val - 1]
                else:
                    out_str += char 
            return out_str
        def ceasar(in_str, n):
            out_str = ""
            for char in in_str:
                if char in self.alphabet:
                    char_val = self.alphabet.find(char)
                    out_str += self.alphabet[(char_val + n) % len(self.alphabet)]
                else:
                    out_str += char 
            return out_str
        def beaufort(in_str, key):
            out_str = ""
            pos = 0
            for char in in_str:
                if char in self.alphabet:
                    key_val = self.alphabet.find(key[pos % len(key)])
                    char_val = self.alphabet.find(char)
                    out_str += self.alphabet[(char_val - key_val) % len(self.alphabet)]
                    pos += 1
                else:
                    out_str += char 
            return out_str
        methods = (a1z26, atbash, ceasar, beaufort)

        # copy cipher_text and method as strings/tuples are immutable
        text = str(self.ciphered)
        method = tuple(method or self.default_method)

        # check if it has been deciphered before
        if method in self.deciphered:
            text = self.deciphered[method]
        # do all decipher methods
        else:
            for i, func in enumerate(methods): 
                if method[i]:
                    text = func(text, method[i])
                    self.append_cipher((*method[:i + 1], *self.default_method[i + 1:]), text)

        return text
    def decipher_all(self):
        for method in self.iter_methods():
            self.decipher(method)

class Prompt():
    def __init__(self, file, prompt_str="cipher> "):
        # copy prompt string as strings are immutable
        self.prompt_str = str(prompt_str)

        self.file = file

    def prompt(self):
        cmd, range_, *_ = input(self.prompt_str).split(" ") + ["", ""]

        Cipher(self.file).print_cipher()
        for cipher in self.handle_range(range_):
            self.handle_command(cmd, cipher)

    def handle_number(self, number):
        if number.find(":") != -1:
            i, j = list(int(i) for i in number.split(":"))
        else:
            i = j = int(number)
        return (i, j + 1)

    def handle_range(self, range_):
        if range_ == "" or range_ == "a" or range_ == "all":
            # use full method ranges
            r = Cipher(self.file).file_ranges
        else:
            # turn the range strings into numbers
            r = list(self.handle_number(i) for i in range_.split("-"))

        # iterate through
        for s in range(*r[0]):
            for e in range(*r[1]):
                for n in range(*r[2]):
                    c = Cipher(self.file, s, e, n)
                    if c.ciphered == "":
                        continue
                    else:
                        yield c
        pass

    def handle_command(self, cmd, cipher: Cipher):
        if cmd == "a" or cmd == "all": # print all methods
            for method in cipher.iter_methods():
                cipher.print_cipher(method)
        elif cmd == "b" or cmd == "best": # print best method
            cipher.print_cipher(cipher.best_cipher())
            pass
        elif cmd[:1] == "r" or cmd[:4] == "rank": # rank all/n methods
            # extract number
            n = cmd.lower().strip("rank")
            try:
                # check if number follows command
                n = int(n)
            except ValueError:
                # else print all methods
                n = None

            # print best num methods
            for method in cipher.rank_methods()[:n]:
                cipher.print_cipher(method)
        pass

if __name__ == "__main__":
    p = Prompt("ciphers.txt", "? ")
    while True:
        p.prompt()